Ensure the following dependencies are met by running
```bash
sudo apt install -y python-pip python-dev
sudo pip install scipy numpy pandas 
```

To run the output, do the following
1. Ensure the packages copied to the "src" folder
2. Build catkin workspace
3. Source the devel/source.sh file
4. Launch the following launch file:
```bash
roslaunch proj_sim proj_demo.launch
```

After launching the file, two vehicle markers will appear. One of them corresponds to the actual vehicle location, while the second marker corresponds to the EKF estimate. The blue line is the vehicle actual trajectory, and the gray marker is the charging pad. 

If desired, the output of the battery state can be viewed in the terminal by issuing the command:
```bash
rostopic echo /battery/battery_state
```
