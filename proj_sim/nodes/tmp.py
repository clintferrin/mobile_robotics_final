#!/usr/bin/env python

import rospy
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point

rospy.init_node('line_pub_example')
pub_line_min_dist = rospy.Publisher('/x_path', Marker, queue_size=1)

marker = Marker()
marker.header.frame_id = "/origin"
marker.type = marker.LINE_STRIP
marker.action = marker.ADD

# marker scale
marker.scale.x = 0.1
marker.scale.y = 0.1
# marker.scale.y = 0.1

# marker color
marker.color.a = 1.0
marker.color.r = 1.0
marker.color.g = 1.0
marker.color.b = 0.0

num_points = 5
tmp = Point()
tmp.x = 0
tmp.y = 0
marker.points.append(tmp)

tmp2 = Point()
tmp2.x = 3
tmp2.y = 2

marker.points.append(tmp2)

# marker.points[2].x = -1
# marker.points[2].y = -3

while not rospy.is_shutdown():
    pub_line_min_dist.publish(marker)
    rospy.sleep(0.25)
