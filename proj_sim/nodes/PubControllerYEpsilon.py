#! /usr/bin/env python

import Parameters
import rospy
from numpy import array
from proj_sim.msg import vehicle_inputs, bicycle_state, velocity_trajectory, path_trajectory
from ControllerYEpsilon import ControllerYEpsilon
from utils import Trajectory2D, Trajectory1D
from PathPlannerFinal import PathPlanner
import sys


def main():
    params = Parameters.Parameters()

    if len(sys.argv) <= 2:
        params.trajectory_path = "../data/trajectories/outter_path_final.traj"
    else:
        params.trajectory_path = sys.argv[2]

    params.trajectory = Trajectory2D().load_binary(params.trajectory_path)

    planner = PathPlanner(params)

    controller = ControllerYEpsilon(params)
    controller.set_trajectory(planner.trajectory)
    controller.control_variation.set_K_delta(params.default_variation_delta / 2.0)

    pub_controller = PubControllerYEpsilon(controller, params)
    pub_controller.publish(hz=params.pub_controller_freq)


class PubControllerYEpsilon:
    def __init__(self, controller, params):
        self.controller = controller
        self.x_hat = array([[0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0]])
        self.pos_trajectory = Trajectory2D(ds=params.dt)

        rospy.init_node('controller', anonymous=True)
        rospy.Subscriber("x_state", bicycle_state, self.__x_state_callback)

    def __x_state_callback(self, data):
        self.x_hat[0, 0] = data.x
        self.x_hat[1, 0] = data.y
        self.x_hat[2, 0] = data.psi
        self.x_hat[3, 0] = data.v
        self.x_hat[4, 0] = data.phi
        self.x_hat[5, 0] = data.b_v
        self.x_hat[6, 0] = data.b_phi

    def publish(self, hz=100):
        pub = rospy.Publisher('control_input', vehicle_inputs, queue_size=10)
        rate = rospy.Rate(hz)  # hz
        u_pub = vehicle_inputs()
        while not rospy.is_shutdown():
            x_hat = self.x_hat.copy()
            x_hat_control = x_hat[0:5, :]
            end_of_traj = self.controller.trajectory_end()

            if end_of_traj:
                u = self.controller.stop_protocol(x_hat_control)
            else:
                u = self.controller.calc_variation_input(x_hat_control)
                # u = self.controller.stop_protocol(x_hat_control)

            u_pub.accel = u[0, 0]
            u_pub.change_steering_angle = u[1, 0]
            u_pub.header.stamp = rospy.get_rostime()
            log_msg = "\nVehicle input: \n"
            log_msg = log_msg + "   throttle: " + str(u_pub.accel)
            log_msg = log_msg + "   steering: " + str(u_pub.change_steering_angle)
            rospy.loginfo(log_msg)
            pub.publish(u_pub)
            rate.sleep()


if __name__ == '__main__':
    main()
