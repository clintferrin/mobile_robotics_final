#! /usr/bin/env python

import Parameters
import SensorSuite as ss
import rospy
from numpy import array
from geometry_msgs.msg import PoseStamped
from KalmanFilter import KalmanFilter
from SensorSuite import SensorSuite
from proj_sim.msg import bicycle_state
from std_msgs.msg import Float64
from tf.transformations import quaternion_from_euler

def main():
    params = Parameters.Parameters()
    sensors = SensorSuite(params)
    ekf = KalmanFilter(sensors, params)
    pub_ekf = PubEkf(ekf)
    pub_ekf.publish(hz=params.pub_ekf_freq)


class PubEkf:
    def __init__(self, ekf):
        self.ekf = ekf
        self.z_tilde = array([[0.0], [0.0]])
        self.v_tilde = 0.0
        self.phi_tilde = 0.0
        self.discrete_update = False

        self.x_hat_output = bicycle_state()
        rospy.init_node('ekf', anonymous=True)

        rospy.Subscriber('z_tilde', PoseStamped, self.__z_tilde_update)
        rospy.Subscriber('phi_tilde', Float64, self.__phi_tilde_update)
        rospy.Subscriber('v_tilde', Float64, self.__v_tilde_update)

    def __z_tilde_update(self, data):
        self.z_tilde[0, 0] = data.pose.position.x
        self.z_tilde[1, 0] = data.pose.position.y
        self.discrete_update = True

    def __phi_tilde_update(self, data):
        self.phi_tilde = data.data

    def __v_tilde_update(self, data):
        self.v_tilde = data.data

    def __package_output(self, x_hat):
        self.x_hat_output.header.stamp = rospy.Time.now()
        self.x_hat_output.x = x_hat[0, 0]
        self.x_hat_output.y = x_hat[1, 0]
        self.x_hat_output.psi = x_hat[2, 0]
        self.x_hat_output.v = self.v_tilde
        self.x_hat_output.phi = self.phi_tilde
        self.x_hat_output.b_v = x_hat[3, 0]
        self.x_hat_output.b_phi = x_hat[4, 0]

        q = quaternion_from_euler(0, 0, x_hat[2, 0])
        self.x_hat_output.pose.orientation.x = q[0]
        self.x_hat_output.pose.orientation.y = q[1]
        self.x_hat_output.pose.orientation.z = q[2]
        self.x_hat_output.pose.orientation.w = q[3]
        self.x_hat_output.pose.position.x = x_hat[0, 0]
        self.x_hat_output.pose.position.y = x_hat[1, 0]
        self.x_hat_output.pose.position.z = 0
        return self.x_hat_output

    def publish(self, hz=100):
        pub = rospy.Publisher('x_hat', bicycle_state, queue_size=10)
        rate = rospy.Rate(hz)  # hz
        while not rospy.is_shutdown():
            y_tilde = array([self.v_tilde, self.phi_tilde]).copy()
            x_hat, P, resid_P = self.ekf.propagate(y_tilde)

            if self.discrete_update:
                z_tilde = self.z_tilde.copy()
                x_hat, P, resid, resid_P = self.ekf.update(z_tilde)
                self.discrete_update = False

            output = self.__package_output(x_hat)
            pub.publish(output)
            rate.sleep()


if __name__ == '__main__':
    main()
