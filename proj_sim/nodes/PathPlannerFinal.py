#! /usr/bin/env python
# coding=utf-8

from numpy import eye, array, c_, r_, array, diag, cos, sin, tan
from utils import Trajectory2D, Trajectory1D
import numpy as np
import Parameters as pm
import VehicleSim as ve
from utils import Trajectory2D
import matplotlib.pyplot as plt
import utils as ut
import scipy.stats


def main():
    # initialization
    params = pm.Parameters()
    sim = ve.VehicleSim(params)
    path_planner = PathPlanner(params)


class PathPlanner:
    def __init__(self, params):
        self.dt = params.dt
        self.trajectory = Trajectory2D().load_binary(params.trajectory_path)
        self.end_time = (len(self.trajectory.s) - 1) * params.dt
        self.desired_end_wh = params.desired_end_wh
        self.q_d_p = params.desired_end_wh_prob
        self.__watt_seconds_to_watt_hours = 1 / float(60.0 * 60.0)
        self.charging_coil_power_transfer = params.charging_coil_power_transfer

        # define driving profile
        self.total_dist = self.trajectory.s[-1]
        self.analysis_distance = 5

        # select transition points for charging pad. charging pad is between: 3-4
        waypoint_charging_indices = params.waypoint_charging_indicies
        self.num_pads = len(waypoint_charging_indices)
        self.pad_idx = 0
        self.pads_dist_idx = []
        self.pads_loc = []
        self.pads_analysis_loc = []
        self.pad_analysis_dist = []
        self.pad_charge_time = []

        # calculate the charging and analysis regions. Program assumes straight charging regions
        for charge_idx in waypoint_charging_indices:
            # calculate number of transition in transition list for waypoint charging pads
            pad1_start_idx = (charge_idx + 1) * 3 - 1
            pad1_end_idx = (charge_idx + 2) * 3 - 1

            # store information so that it can be accessed by pads_loc
            self.pads_dist_idx.append([self.trajectory.transitions[pad1_start_idx],
                                       self.trajectory.transitions[pad1_end_idx]])

            # store start and end locations of trajectory distance
            self.pads_loc.append([self.trajectory.s[self.pads_dist_idx[-1][0]],
                                  self.trajectory.s[self.pads_dist_idx[-1][1]]])

            # calculate 10 seconds before charing pad for analysis region
            charge_velocity = self.trajectory.v[pad1_start_idx]
            ten_seconds_to_idx = int(round(charge_velocity / float(params.dt)))
            # charging analysis
            self.pads_analysis_loc.append([self.trajectory.x[self.pads_dist_idx[-1][0] - ten_seconds_to_idx],
                                           self.trajectory.y[self.pads_dist_idx[-1][0] - ten_seconds_to_idx],
                                           "charge"])
            # stop charging region
            self.pads_analysis_loc.append([self.trajectory.x[self.pads_dist_idx[-1][1]],
                                           self.trajectory.y[self.pads_dist_idx[-1][1]],
                                           "end_charge"])

            self.pad_analysis_dist.append(self.trajectory.s[-1] -
                                          self.trajectory.s[self.pads_dist_idx[-1][0] - ten_seconds_to_idx])
            v = self.trajectory.v[-1]
            self.pad_charge_time.append((self.trajectory.s[self.pads_dist_idx[-1][1]] -
                                         self.trajectory.s[self.pads_dist_idx[-1][0]]) / v)

    def check_update_K_type(self, x, y):
        if self.pad_idx is -1:
            return False

        analysis_x = self.pads_analysis_loc[self.pad_idx][0]
        analysis_y = self.pads_analysis_loc[self.pad_idx][1]
        charge_type = self.pads_analysis_loc[self.pad_idx][2]

        analysis_dist = np.sqrt((analysis_x - x) ** 2 - (analysis_y - y) ** 2)
        if analysis_dist < self.analysis_distance and charge_type is "charge":
            self.pad_idx = self.pad_idx + 1
            return "charge"

        elif analysis_dist < self.analysis_distance and charge_type is "end_charge":
            self.pad_idx = self.pad_idx + 1
            if self.pad_idx > 2 * self.num_pads - 1:
                self.pad_idx = -1
            return "end_charge"

        else:
            return False

    def calc_desired_avg_charge(self, charge_pad_idx, current_charge, desired_charge, avg_power_10_sec):
        # calculate distance to destination
        total_dist = self.pad_analysis_dist[charge_pad_idx / 2] + self.analysis_distance

        # find length of charging region:
        avg_vel = np.average(self.trajectory.v)
        charge_time = sum(self.pad_charge_time[(charge_pad_idx / 2):])

        # estimated battery reduction
        watts_per_v = np.average(avg_power_10_sec)
        est_required_watt_hours = watts_per_v * total_dist / avg_vel * self.__watt_seconds_to_watt_hours

        # possible charge available
        max_wh = self.charging_coil_power_transfer * charge_time * self.__watt_seconds_to_watt_hours

        # estimate wh needed
        wh_needed = desired_charge - (current_charge - est_required_watt_hours)
        wh_needed = min(max(wh_needed, 0), max_wh)
        q_d_avg = 3600 * wh_needed / float(charge_time)
        q_d_avg = min(q_d_avg, self.charging_coil_power_transfer)
        return q_d_avg

    def calc_z_score(self, prob, CI=True):
        if CI:
            z = scipy.stats.norm.ppf((1 + prob) / 2.0)
        else:
            z = scipy.stats.norm.ppf(prob)
        return z

    plt.show()


if __name__ == '__main__':
    main()
