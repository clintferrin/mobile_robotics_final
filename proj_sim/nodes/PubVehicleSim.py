#! /usr/bin/env python

import Parameters
import rospy
from numpy import array, sign
from proj_sim.msg import bicycle_state
from tf.transformations import quaternion_from_euler
from VehicleSim import VehicleSim
from utils import calc_resistive_accel
from proj_sim.msg import vehicle_inputs


def main():
    params = Parameters.Parameters()
    sim = VehicleSim(params)
    pub_sim = PubVehicleSim(sim, params)
    pub_sim.publish(hz=params.pub_vehicle_sim_freq)


class PubVehicleSim:
    def __init__(self, sim, params):
        self.sim = sim
        self.params = params
        self.msg = bicycle_state()
        self.u = array([0.0, 0.0])
        self.x_output = bicycle_state()
        rospy.init_node('vehicle_sim', anonymous=True)
        rospy.Subscriber('control_input', vehicle_inputs, self.__accel_update)

    def __accel_update(self, data):
        self.header = data.header
        self.u[0] = data.accel
        self.u[1] = data.change_steering_angle

    def __package_output(self, x, distance_traveled):
        self.x_output.header.stamp = rospy.Time.now()
        self.x_output.x = x[0, 0]
        self.x_output.y = x[1, 0]
        self.x_output.psi = x[2, 0]
        self.x_output.v = x[3, 0]
        self.x_output.phi = x[4, 0]
        self.x_output.b_v = x[5, 0]
        self.x_output.b_phi = x[6, 0]
        self.x_output.distance_traveled = distance_traveled

        q = quaternion_from_euler(0, 0, x[2, 0])
        self.x_output.pose.orientation.x = q[0]
        self.x_output.pose.orientation.y = q[1]
        self.x_output.pose.orientation.z = q[2]
        self.x_output.pose.orientation.w = q[3]
        self.x_output.pose.position.x = x[0, 0]
        self.x_output.pose.position.y = x[1, 0]
        self.x_output.pose.position.z = 0
        return self.x_output

    def publish(self, hz=100):
        pub_x = rospy.Publisher('x_state', bicycle_state, queue_size=10)
        rate = rospy.Rate(hz)  # hz
        while not rospy.is_shutdown():
            u = self.u.copy()  # ensure u does not change during propagate
            u[0] = u[0] - sign(self.sim.x[3, 0]) * calc_resistive_accel(self.sim.x[3, 0], self.params, incline=0)

            x = self.sim.propagate(u)
            distance_traveled = self.sim.distance_traveled
            log_str = "\nTime: %s" % rospy.get_time() + \
                      "\ninput: " + str(self.u.T) + "\n" + \
                      "state: \n" + str(x) + "\n"
            # rospy.loginfo(log_str)
            x_msg = self.__package_output(x, distance_traveled)
            pub_x.publish(x_msg)
            rate.sleep()


if __name__ == '__main__':
    main()
