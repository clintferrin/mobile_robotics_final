#! /usr/bin/env python

import Parameters
import SensorSuite as ss
import rospy
from numpy import array, float64
from proj_sim.msg import bicycle_state
import std_msgs.msg as std_msgs
import sys

def main():
    # validate argument parameters
    params = Parameters.Parameters()
    sensors = ss.SensorSuite(params)
    steering_angle_measurement = PubSensorSteering(sensors)
    steering_angle_measurement.publish(hz=params.pub_steering_angle_sensor_freq)


class PubSensorSteering:
    def __init__(self, sensors):
        self.sensors = sensors
        self.x = array([[0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0]], dtype=float64)
        rospy.init_node('phi_tilde', anonymous=True)
        rospy.Subscriber("x_state", bicycle_state, self.__callback)

    def __callback(self, data):
        self.x[4, 0] = data.phi
        self.x[6, 0] = data.b_phi

    def publish(self, hz=100):
        pub = rospy.Publisher('phi_tilde', std_msgs.Float64, queue_size=10)
        rate = rospy.Rate(hz)  # hz
        while not rospy.is_shutdown():
            phi_tilde = float64(self.sensors.get_steering_angle(self.x)).copy()
            log_str = "phi_tilde: " + str(phi_tilde)
            rospy.loginfo(log_str)
            pub.publish(phi_tilde)
            rate.sleep()


if __name__ == '__main__':
    main()
