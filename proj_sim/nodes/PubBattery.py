#! /usr/bin/env python

import Parameters
import rospy
from numpy import array
from BatteryModel import BatteryModel
from proj_sim.msg import bicycle_state, vehicle_inputs, battery_state
from utils import Trajectory2D, Trajectory1D
import sys


def main():
    if len(sys.argv) == 1:
        path_to_charging_coil_list = "../data/single_coil.csv"
    else:
        path_to_charging_coil_list = sys.argv[1]

    params = Parameters.Parameters()
    battery = BatteryModel(path_to_charging_coil_list, params)
    pub_battery = PubBattery(battery, params)
    pub_battery.publish(hz=params.pub_battery_freq)


class PubBattery:
    def __init__(self, battery, params):
        self.battery = battery
        self.dt = battery.dt
        self.x = params.x0[0]
        self.y = params.x0[1]
        self.v = params.x0[3]
        self.distance_traveled = 0
        self.accel = 0
        self.change_steering_angle = 0

        self.incline = params.driving_incline
        self.dt = params.dt

        rospy.init_node('battery', anonymous=True)
        rospy.Subscriber("x_state", bicycle_state, self.__x_state_callback)
        rospy.Subscriber("control_input", vehicle_inputs, self.__control_input)

    def __x_state_callback(self, msg):
        self.x = msg.x
        self.y = msg.y
        self.v = msg.v
        self.distance_traveled = msg.distance_traveled

    def __control_input(self, data):
        self.accel = data.accel
        self.change_steering_angle = data.change_steering_angle

    def publish(self, hz=100):
        pub = rospy.Publisher('battery_state', battery_state, queue_size=10)
        rate = rospy.Rate(hz)  # hz
        battery_msg = battery_state()
        u = [0, 0]

        while not rospy.is_shutdown():
            x = self.x
            y = self.y
            v = self.v
            accel = self.accel
            change_steering_angle = self.change_steering_angle
            distance_traveled = self.distance_traveled
            u = [accel, change_steering_angle]
            # update battery state of charge after dynamic charging
            battery_msg.header.stamp = rospy.get_rostime()

            battery_usage = self.battery.update_battery_loss(v, u[0], u[1], self.dt, incline=self.incline)
            battery_charge = self.battery.update_dynamic_charging(x, y)

            battery_msg.battery_usage = battery_usage
            battery_msg.dynamic_charge_watt_hours = battery_charge
            battery_msg.state_of_charge = self.battery.state_of_charge
            battery_msg.avg_kWh_per_km = self.battery.state_of_charge / float(distance_traveled + 0.00001) * 1000

            pub.publish(battery_msg)
            rate.sleep()


if __name__ == '__main__':
    main()
