# coding=utf-8
from numpy import deg2rad, pi, random, sqrt


class Parameters:
    def __init__(self):
        # simulator
        self.x0 = [0, 0, 0, 10, 0]  # [x y ψ v Φ]=[x-pos, y-pos, heading, velocity, steering angle]
        self.simulations = 3
        self.end_time = 20  # seconds
        self.dt = 0.01  # integration time step
        self.noise = False  # turns sensor noise on/off (True|False)
        self.vel_noise = True  # turns velocity sensor noise on/off (True|False)
        self.phi_noise = False  # turns steering sensor noise on/off (True|False)
        self.gps_noise = True  # turns discrete sensor noise on/off (True|False)
        self.seed = random.seed(1)  # random seed for consistent noise simulations

        # velocity sensor
        self.v_sensor_sig = 0.05  # standard deviation of velocity sensor noise
        self.tau_v = 100.  # time constant for sensor bias
        self.v_bias_sig_ss = 0.1  # steady state bias

        # steering angle sensor
        self.phi_sensor_sig = deg2rad(0.08 / 10)  # (radians) standard deviation of steering angle sensor noise
        self.tau_phi = 100.  # time constant for sensor bias
        self.phi_bias_sig_ss = deg2rad(0.2)  # steady state bias

        # gps sensor
        self.gps_x_sig = .03  # meters
        self.gps_y_sig = .03  # meters
        self.gps_freq = 1  # hz
        self.gps_distance_from_back_axel = .25  # meters

        # vehicle
        self.wheel_base = 2.64922  # meters
        self.wheel_track = 1.5  # meters (measured in Blender)
        self.max_steer_angle = deg2rad(35)  # phi
        self.max_change_steer_angle = deg2rad(45)  # per second
        self.max_velocity_limit = 40  # m/s
        self.max_accel_limit = 4  # m/s^2
        self.wheel_radius = 0.3  # meters

        # initial state covariance
        # NOTE: the initial condition for psi is unreasonable without additional hardware (i.e., magnatometer).
        # Psi is set to 1 degree knowledge so that the covariance does not have to be beaten down at the begning of the
        # simulation
        self.x1_sig_init = self.gps_x_sig
        self.x2_sig_init = self.gps_y_sig
        self.psi_sig_init = deg2rad(1)  # 1 degree is an unreasonable initial condition. See NOTE
        self.v_b_sig_init = self.v_bias_sig_ss
        self.phi_b_sig_init = self.phi_bias_sig_ss

        # velocity controller
        self.v_error_epsilon = 1  # recalculate velocity path if error > epsilon
        self.ds = 0.01  # point distances for location
        self.nominal_control_speed = 29.0576  # 60 mph = 26.8224
        self.time_to_nominal = 10  # sec

        # steering angle controller and path planner
        self.steering_resistance = 1  # N s/radians
        self.control_reference = 5  # from back axel reference point

        # path planner settings
        self.trajectory_path = "../data/trajectories/outter_path_final.traj"
        self.path_to_charging_coil_list = "../data/final_test.csv"
        self.desired_end_wh = 15300  # watts
        self.desired_end_wh_prob = .97  # probability interval
        self.waypoint_charging_indicies = [2, 4]

        # vehicle battery
        self.max_battery_capacity = 23000  # wH
        self.initial_charge = 15000  # starting charge value in wH
        self.charging_coil_power_transfer = 60000  # Watts
        self.regenerative_charging_transfer_coefficient = .85

        # stochastic variation paramters
        self.include_control_variation = True
        self.desired_avg_charge = 40000  # Watts
        self.desired_avg_charge_p = 0.97  # probability of desired charge
        self.default_variation_delta = 0.3  # 3 sigma_delta
        self.lane_end = .75  # ultimate clipped bound
        self.analysis_start_time = 10  # seconds
        self.zeta = 1 / sqrt(2)
        self.comfort_cutoff_frequency = 0.2 * 2 * pi  # comfort cutoff frequency
        self.sig2_delta = 0.3457  # maximum sig2_delta = 0.3457
        self.sig2_alpha = 0.16  # maximum sig2_alpha = 0.16

        # resistive force parameters (Ford Focus Profile)
        self.front_area = 2.24546551  # m^2
        self.drag_coefficient = 0.274
        self.mass = 1643.819 + 181  # kilograms (3624 lbs)
        self.mass_factor = 1.05
        self.tire_pressure = 2.41317  # bar 35 psi
        self.air_density = 1.225  # air density as sea level kg/m^2
        self.driving_incline = deg2rad(0)  # incline of road. negative is downhill
        self.tire_friction_coef = 0.5  # 0.7 for dry to 0.4 for wet (Cite hyperphysics)

        # ros frequency settings
        self.pub_ekf_freq = float(1 / self.dt)
        self.pub_controller_freq = float(1 / self.dt)
        self.pub_planner_freq = float(1 / self.dt)
        self.pub_vehicle_marker_freq = 100
        self.pub_vehicle_sim_freq = float(1 / self.dt)
        self.pub_battery_freq = float(1 / self.dt)
        self.pub_velocity_sensor_freq = 100.
        self.pub_steering_angle_sensor_freq = 100.

        # # resistive force parameters (example car)
        # self.resistive_force_parameters = True  # turns resistive force on/off (True|False). applies only to ROS
        # self.front_area = 2  # m^2
        # self.mass = 1360  # kg - weight is
        # self.mass_factor = 1.05
        # self.drag_coefficient = 0.5
        # self.tire_pressure = 2.41317  # bar - 35 psi
        # self.air_density = 1.225  # kg/m^2 - air density as sea level
        # self.driving_incline = deg2rad(0)
