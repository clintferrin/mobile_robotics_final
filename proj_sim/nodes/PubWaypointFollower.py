#!/usr/bin/env python
import rospy
import proj_sim.msg as vs
from visualization_msgs.msg import Marker, MarkerArray
from Parameters import Parameters
from geometry_msgs.msg import PoseStamped
from ControllerYEpsilon import ControllerYEpsilon
from tf.transformations import euler_from_quaternion
from proj_sim.msg import vehicle_inputs, bicycle_state
import numpy as np


def main():
    params = Parameters()
    controller = ControllerYEpsilon(params)
    args = {"controller": controller}
    waypoint_controller = PublishWaypointControl(args)
    waypoint_controller.publish(hz=10)


class PublishWaypointControl:
    def __init__(self, args):
        rospy.init_node('waypoint_follower', anonymous=True)
        rospy.Subscriber("/move_base_simple/goal", PoseStamped, self.__update_goal)
        rospy.Subscriber("x_state", bicycle_state, self.__x_state_callback)

        self.controller = args["controller"]
        self.x_hat = np.array([[0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0]])
        self.x_d = 0
        self.y_d = 0
        self.theta_d = 0

    def publish(self, hz=100):
        pub = rospy.Publisher('control_input', vehicle_inputs, queue_size=10)
        rate = rospy.Rate(hz)  # hz
        u_pub = vehicle_inputs()
        while not rospy.is_shutdown():
            yd = [self.x_d, self.y_d]
            yd_dot = [0, 0]
            u = self.controller.calc_input_yd(self.x_hat, yd, yd_dot)
            u_pub.accel = u[0, 0]
            u_pub.change_steering_angle = u[1, 0]

            pub.publish(u_pub)
            rate.sleep()

    def __update_goal(self, msg):
        self.x_d = msg.pose.position.x
        self.y_d = msg.pose.position.y
        q = msg.pose.orientation
        q_list = [q.x, q.y, q.z, q.w]
        (_, _, self.theta_d) = euler_from_quaternion(q_list)

    def __x_state_callback(self, data):
        self.x_hat[0, 0] = data.x
        self.x_hat[1, 0] = data.y
        self.x_hat[2, 0] = data.psi
        self.x_hat[3, 0] = data.v
        self.x_hat[4, 0] = data.phi
        self.x_hat[5, 0] = data.b_v
        self.x_hat[6, 0] = data.b_phi


if __name__ == '__main__':
    main()
