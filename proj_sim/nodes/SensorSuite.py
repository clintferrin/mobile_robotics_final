#! /usr/bin/env python
# coding=utf-8

import numpy as np
from numpy import diag, sqrt, pi
from numpy.random import normal
import VehicleSim as vs
import Parameters
from utils import rotation_mat


def main():
    params = Parameters.Parameters()



class SensorSuite:
    def __init__(self, params):
        # obtain necessary function information from params
        self.dt = params.dt
        self.gps_freq = params.gps_freq

        # variable sizing parameters
        self.L = params.wheel_base
        self.gps_ref = params.gps_distance_from_back_axel

        # size Q for sensor noise
        q_v = params.v_sensor_sig * self.dt  # times dt because noise is scaled by noise multiplication factors
        q_phi = params.phi_sensor_sig * self.dt  # times dt because noise is scaled by noise multiplication factors
        q_v_bias = 2 * params.v_bias_sig_ss ** 2 / float(params.tau_v)
        q_phi_bias = 2 * params.phi_bias_sig_ss ** 2 / float(params.tau_phi)

        # build Q matrix
        self.Q = diag([q_v,
                       q_phi,
                       q_v_bias,
                       q_phi_bias])

        # build R matrix for discrete sensors
        self.gps_x_sig = params.gps_x_sig
        self.gps_y_sig = params.gps_y_sig

        self.R = diag([self.gps_x_sig ** 2,
                       self.gps_y_sig ** 2])

        # calculate noise multiplication factors
        self.v_noise_scale = sqrt(self.Q[0, 0] / self.dt)
        self.phi_noise_scale = sqrt(self.Q[1, 1] / self.dt)

        # turn off noise
        if not params.noise or not params.vel_noise:
            self.v_noise_scale = 0
        if not params.noise or not params.phi_noise:
            self.phi_noise_scale = 0
        if not params.noise or not params.gps_noise:
            self.gps_x_sig = 0
            self.gps_y_sig = 0

    def get_velocity(self, x):
        return x[3, 0] + x[5, 0] + normal() * self.v_noise_scale

    def get_steering_angle(self, x):
        return x[4, 0] + x[6, 0] + normal() * self.phi_noise_scale

    def get_gps_pos(self, x):
        # rotate gps location into reference frame
        gps_ref = rotation_mat(x[2, 0]).dot(np.array([[self.gps_ref], [0]]))
        gps_pos = x[0:2] + gps_ref + np.array([[normal() * self.gps_x_sig],
                                               [normal() * self.gps_y_sig]])
        return gps_pos


if __name__ == '__main__':
    main()
