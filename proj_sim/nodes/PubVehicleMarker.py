#!/usr/bin/env python
import rospy
import tf2_ros
import sys
import proj_sim.msg as vs
import geometry_msgs.msg as geo
import numpy as np
from visualization_msgs.msg import Marker, MarkerArray
from Parameters import Parameters
from tf.transformations import quaternion_from_euler


def main():
    params = Parameters()

    # validate argument parameters
    if len(sys.argv) >= 4:
        args = {"tf_parent": sys.argv[1],
                "tf_child": sys.argv[2],
                "marker_path": sys.argv[3],
                "params": params}
    else:
        args = {"tf_parent": "origin",
                "tf_child": "vehicle",
                "marker_path": "../markers/dae_files",
                "params": params}
    vehicle_marker = PublishVehicleMarker(args)
    rospy.spin()


class PublishVehicleMarker:
    def __init__(self, args):
        rospy.init_node('vehicle_marker', anonymous=True)
        rospy.Subscriber("x_state", vs.bicycle_state, self.__publish_marker)
        self.params = args["params"]
        self.dt = self.params.dt
        self.r = self.params.wheel_radius
        self.L = self.params.wheel_base
        self.T = self.params.wheel_track

        self.front_wheel_distance_from_ref = 3.155
        self.wheel_height = .37
        self.tire_y_offset = .9
        self.hubcap_y_offset = .86

        self.trans = geo.TransformStamped()
        self.tf_parent = args["tf_parent"]
        self.tf_child = args["tf_child"]
        self.marker_path = args["marker_path"]
        self.trans.header.frame_id = self.tf_parent
        self.trans.child_frame_id = self.tf_child
        self.br = tf2_ros.TransformBroadcaster()
        self.wheel_twist = 0
        self.num_markers = 11
        self.scale = 0.8229
        self.m_array = MarkerArray()
        self.m_array.markers = [Marker() for _ in range(self.num_markers)]

        # publish marker
        self.marker_pub = rospy.Publisher(self.tf_child, MarkerArray, queue_size=100)
        for idx in range(self.num_markers):
            self.m_array.markers[idx].header.frame_id = self.tf_child
            self.m_array.markers[idx].header.stamp = rospy.get_rostime()
            self.m_array.markers[idx].frame_locked = True
            self.m_array.markers[idx].id = idx
            self.m_array.markers[idx].type = 10
            self.m_array.markers[idx].action = 0
            self.m_array.markers[idx].mesh_use_embedded_materials = True
            self.m_array.markers[idx].color.r = 0
            self.m_array.markers[idx].color.g = 0
            self.m_array.markers[idx].color.b = 0
            self.m_array.markers[idx].color.a = 1
            self.m_array.markers[idx].scale.x = self.scale
            self.m_array.markers[idx].scale.y = self.scale
            self.m_array.markers[idx].scale.z = self.scale

        # body attributes
        self.m_array.markers[0].mesh_resource = self.marker_path + "/ford2012_body_no_wheels.dae"
        self.m_array.markers[1].mesh_resource = self.marker_path + "/ford2012_windows.dae"
        self.m_array.markers[0].color.r = .95
        self.m_array.markers[0].color.g = .95
        self.m_array.markers[0].color.b = .95
        # accent colors (windows, lights... etc)
        self.m_array.markers[1].color.a = .65

        # general wheel and hub cap attributes
        hub_idx = [4, 6, 8, 10]
        for idx in hub_idx:
            self.m_array.markers[idx].mesh_resource = self.marker_path + "/ford2012_left_hub_cap.dae"
            self.m_array.markers[idx].color.r = 1
            self.m_array.markers[idx].color.g = 1
            self.m_array.markers[idx].color.b = 1

        # wheel attributes
        wheel_idx = [3, 5, 7, 9]
        for idx in wheel_idx:
            self.m_array.markers[idx].mesh_resource = self.marker_path + "/ford2012_left_wheel.dae"
            self.m_array.markers[idx].color.a = .85

        # position attributes for wheels and hubcaps
        # left back wheel and hubcap positions and orientation
        wheel = 3
        hubcap = 4
        self.m_array.markers[wheel].pose.position.y = self.tire_y_offset * self.scale
        self.m_array.markers[wheel].pose.position.z = self.wheel_height * self.scale
        self.m_array.markers[hubcap].pose.position.y = self.hubcap_y_offset * self.scale
        self.m_array.markers[hubcap].pose.position.z = self.wheel_height * self.scale

        # right back wheel and hubcap
        wheel = 5
        hubcap = 6
        self.m_array.markers[wheel].pose.position.y = -self.tire_y_offset * self.scale
        self.m_array.markers[wheel].pose.position.z = self.wheel_height * self.scale
        self.m_array.markers[hubcap].pose.position.y = -self.hubcap_y_offset * self.scale
        self.m_array.markers[hubcap].pose.position.z = self.wheel_height * self.scale

        # left front wheel and hubcap
        wheel = 7
        hubcap = 8
        self.m_array.markers[wheel].pose.position.x = self.front_wheel_distance_from_ref * self.scale
        self.m_array.markers[wheel].pose.position.y = self.tire_y_offset * self.scale
        self.m_array.markers[wheel].pose.position.z = self.wheel_height * self.scale
        self.m_array.markers[hubcap].pose.position.x = self.front_wheel_distance_from_ref * self.scale
        self.m_array.markers[hubcap].pose.position.y = self.hubcap_y_offset * self.scale
        self.m_array.markers[hubcap].pose.position.z = self.wheel_height * self.scale

        # left front wheel and hubcap
        wheel = 9
        hubcap = 10
        self.m_array.markers[wheel].pose.position.x = self.front_wheel_distance_from_ref * self.scale
        self.m_array.markers[wheel].pose.position.y = -self.tire_y_offset * self.scale
        self.m_array.markers[wheel].pose.position.z = self.wheel_height * self.scale
        self.m_array.markers[hubcap].pose.position.x = self.front_wheel_distance_from_ref * self.scale
        self.m_array.markers[hubcap].pose.position.y = -self.hubcap_y_offset * self.scale
        self.m_array.markers[hubcap].pose.position.z = self.wheel_height * self.scale

    def __publish_marker(self, msg):
        self.trans.header.stamp = msg.header.stamp
        self.trans.transform.translation = msg.pose.position
        self.trans.transform.rotation = msg.pose.orientation
        self.br.sendTransform(self.trans)
        v = msg.v
        phi = msg.phi

        for idx in range(self.num_markers):
            self.m_array.markers[idx].action = 0
            self.m_array.markers[idx].header.stamp = rospy.get_rostime()
            self.m_array.markers[idx].header.frame_id = self.tf_child

        # spins at the correct revolutions per second.
        self.wheel_twist = self.wheel_twist + 1 / (2 * np.pi * self.r) * v * 1 / 45

        # wheel attributes
        quat = [quaternion_from_euler(0, self.wheel_twist, 0),
                quaternion_from_euler(np.pi, self.wheel_twist, 0),
                quaternion_from_euler(0, self.wheel_twist, phi),
                quaternion_from_euler(np.pi, self.wheel_twist, phi)]

        # left back wheel and hubcap
        wheel = 3
        hubcap = 4
        self.m_array.markers[wheel].pose.orientation = self.__update_orientation(quat[0])
        self.m_array.markers[hubcap].pose.orientation = self.__update_orientation(quat[0])

        # right back wheel and hubcap
        wheel = 5
        hubcap = 6
        self.m_array.markers[wheel].pose.orientation = self.__update_orientation(quat[1])
        self.m_array.markers[hubcap].pose.orientation = self.__update_orientation(quat[1])

        # left front wheel and hubcap
        wheel = 7
        hubcap = 8
        self.m_array.markers[wheel].pose.orientation = self.__update_orientation(quat[2])
        self.m_array.markers[hubcap].pose.orientation = self.__update_orientation(quat[2])

        # left front wheel and hubcap
        wheel = 9
        hubcap = 10
        self.m_array.markers[wheel].pose.orientation = self.__update_orientation(quat[3])
        self.m_array.markers[hubcap].pose.orientation = self.__update_orientation(quat[3])

        self.marker_pub.publish(self.m_array)

    def __update_orientation(self, q):
        marker = Marker()
        marker.pose.orientation.x = q[0]
        marker.pose.orientation.y = q[1]
        marker.pose.orientation.z = q[2]
        marker.pose.orientation.w = q[3]
        return marker.pose.orientation


if __name__ == '__main__':
    main()
