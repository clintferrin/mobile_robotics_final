#! /usr/bin/env python
# coding=utf-8

import Parameters as pm
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats


def main():
    print("main")


class ControlVariation:
    def __init__(self, params):
        self.zeta = params.zeta
        self.comfort_cutoff_frequency = params.comfort_cutoff_frequency  # comfort cutoff frequency
        self.dt = params.dt
        self.x = np.zeros((4, 1))
        self.default_variation_delta = params.default_variation_delta

        # set up dynamic equations
        w_c = self.comfort_cutoff_frequency

        Omega = -2 * w_c ** 2 / (1 / np.sqrt(2))
        w_l = np.sqrt(-np.sqrt(1 / 4.0 * Omega ** 2 - w_c ** 4) - 1 / 2.0 * Omega)
        w_h = w_l

        a1 = 2 * w_h * self.zeta + 2 * w_l * self.zeta
        a2 = w_h ** 2 + 4 * w_h * w_l * self.zeta ** 2 + w_l ** 2
        a3 = 2 * w_h ** 2 * w_l + 2 * w_h * w_l ** 2
        a4 = w_h ** 2 * w_l ** 2
        b = w_l ** 2

        self.delta_sig2_coef = 0.501245175829430
        self.alpha_sig2_coef = 0.071485334682628

        self.K = []
        self.set_K_delta(params.sig2_delta)

        self.A = np.array([[0, 1, 0, 0],
                           [0, 0, 1, 0],
                           [0, 0, 0, 1],
                           [-a4, -a3, -a2, -a1]])

        self.B = np.array([[0], [0], [0], [1]])

        self.C_alpha = np.array([0, 0, b, 0])
        self.C_vel = np.array([0, b, 0, 0])
        self.C_delta = np.array([b, 0, 0, 0])

        # initialize estimating coeficients for charge vs K
        # self.charge_mu_coefs = np.array([10795.991960891779, -55177.79398007155, 70945.58935658819])
        # self.charge_sig_coefs = np.array([-24195.861342693643, 36538.70634802107, -5055.523955397114])

        # developed with sensor noise
        self.charge_mu_coefs = np.array([11287.57488388, -52133.54631698, 67196.09215384])
        self.charge_sig_coefs = np.array([-20829.53417506, 28276.56415646, -1170.11887757])

    def set_K_delta(self, sig2_delta):
        self.K = np.sqrt(sig2_delta / float(self.delta_sig2_coef))
        return self.K

    def set_K_charge(self, desired_charge, z):
        alpha = (self.charge_mu_coefs[1] - z * self.charge_sig_coefs[1]) / \
                (self.charge_mu_coefs[0] - z * self.charge_sig_coefs[0])
        beta = (desired_charge - (self.charge_mu_coefs[2] - z * self.charge_sig_coefs[2])) / \
               (self.charge_mu_coefs[0] - z * self.charge_sig_coefs[0])
        if beta + 1 / 4.0 * alpha ** 2 < 0:
            self.K = self.set_K_delta(self.default_variation_delta)
        else:
            K = -np.sqrt(beta + 1 / 4.0 * alpha ** 2) - 1 / 2.0 * alpha
            self.K = max(0, K)
        return self.K

    def propagate(self):
        x = self.x
        h = self.dt
        hh = h / 2
        h6 = h / 6

        u = np.random.normal(0, 1 / np.sqrt(self.dt))

        dydx = self.__x_dot(x, u)
        yt = x + hh * dydx

        dyt = self.__x_dot(yt, u)
        yt = x + hh * dyt

        dym = self.__x_dot(yt, u)
        yt = x + h * dym
        dym = dyt + dym

        dyt = self.__x_dot(yt, u)
        yout = x + h6 * (dydx + dyt + 2 * dym)
        x_dot = yout
        self.x = x_dot

        alpha = self.K * self.C_alpha.dot(self.x)
        vel = self.K * self.C_vel.dot(self.x)
        delta = self.K * self.C_delta.dot(self.x)

        return alpha, vel, delta

    def __x_dot(self, x, u):
        x_dot = self.A.dot(x) + self.B.dot(u)
        return x_dot

    def so_lp_mag2(self, w, w_l, zeta):
        G = w_l ** 4 / (w ** 4 + 2 * w_l ** 2 * (2 * zeta ** 2 - 1) + w_l ** 4)
        return G

    def so_hp_mag2(self, w, w_h, zeta):
        G = w ** 4 / (w ** 4 + 2 * w_h ** 2 * (2 * zeta ** 2 - 1) + w_h ** 4)
        return G

    def so_lp_s2_mag2(self, w, w_l, zeta):
        G = w ** 4 * w_l ** 4 / (w ** 4 + 2 * w_l ** 2 * (2 * zeta ** 2 - 1) + w_l ** 4)
        return G

    def so_bp_mag2(self, w, w_l, w_h):
        return w_l ** 4 * w ** 4 / ((w ** 4 + w_l ** 4) * (w ** 4 + w_h ** 4))

    def so_bp_s2_mag2(self, w, w_l, w_h):
        return w_l ** 4 / ((w ** 4 + w_l ** 4) * (w ** 4 + w_h ** 4))

    def mag2_to_20_log(self, mag):
        return 10 * np.log10(mag)

    def plot_lp(self, w_l, K, w_start, w_end, ax=None):
        w = np.logspace(w_start, w_end, num=100, endpoint=True)
        self.plot_G(w, K ** 2 * self.so_lp_mag2(w, w_l, 1 / np.sqrt(2)), ax=ax,
                    label="$\left|G_{lp}\left(\omega\\right)\\right|^{2}$")
        plt.scatter(w_l, 20 * np.log10(1 / np.sqrt(2)))

    def plot_hp(self, w_h, K, w_start, w_end, ax=None):
        w = np.logspace(w_start, w_end, num=100, endpoint=True)
        self.plot_G(w, K ** 2 * self.so_hp_mag2(w, w_h, 1 / np.sqrt(2)), ax=ax,
                    label="$\left|G_{lp}\left(\omega\\right)\\right|^{2}$")
        plt.scatter(w_h, 20 * np.log10(1 / np.sqrt(2)))

    def plot_bp(self, w_l, w_h, K, w_start, w_end, ax=None):
        w = np.logspace(w_start, w_end, num=1000, endpoint=True)
        self.plot_G(w, K ** 2 * self.so_bp_mag2(w, w_l, w_h), ax=ax,
                    label="$\left|G_{bp}\left(\omega\\right)\\right|^{2}$")

    def plot_bp_s2(self, w_l, w_h, w_start, w_end, ax=None):
        w = np.logspace(w_start, w_end, num=100, endpoint=True)
        self.plot_G(w, self.so_bp_s2_mag2(w, w_l, w_h), ax=ax,
                    label="$\left|H\left(\omega\\right)\\right|^{2}\cdot$")

    def plot_G(self, w, G, freq=True, ax=None, label=None):
        if ax is None:
            fig, ax = plt.subplots()

        if freq is True:
            if label is None:
                ax.plot(w, self.mag2_to_20_log(G))
            else:
                ax.plot(w, self.mag2_to_20_log(G), label=label)

            ax.set_xscale('log')

        else:
            if label is None:
                ax.plot(w, G)
            else:
                ax.plot(w, G, label=label)

        ax.set_xlabel("Frequency (radians)")
        ax.set_ylabel("Magnitude (20log10)")
        ax.legend()


if __name__ == '__main__':
    main()
