#! /usr/bin/env python
import Parameters
import rospy
from numpy import array
from proj_sim.msg import vehicle_inputs, bicycle_state, velocity_trajectory, path_trajectory
from ControllerYEpsilon import ControllerYEpsilon
from utils import Trajectory2D, Trajectory1D
from PathPlannerFinal import PathPlanner
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
import sys
from proj_sim.msg import bicycle_state
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point

def main():
    # validate argument parameters
    if len(sys.argv) >= 3:
        args = {"tf_parent": sys.argv[1],
                "buff_len": sys.argv[2]}
    else:
        args = {"tf_parent": "origin",
                "buff_len": 1500}

    vehicl_path = PubVehiclePath(args)
    rospy.spin()


class PubVehiclePath:
    def __init__(self, args):
        rospy.init_node('vehicle_path', anonymous=True)
        rospy.Subscriber("x_state", bicycle_state, self.__publish_path)
        self.pub_path = rospy.Publisher('/x_path', Marker, queue_size=1)
        self.buff_len = args["buff_len"]

        self.marker = Marker()
        self.marker.header.frame_id = "/origin"
        self.marker.type = self.marker.LINE_STRIP
        self.marker.action = self.marker.ADD

        # marker scale
        self.marker.scale.x = 0.1
        self.marker.scale.y = 0.1

        # marker color
        self.marker.color.a = 1.0
        self.marker.color.r = 0.145
        self.marker.color.g = 0.498
        self.marker.color.b = 0.784

        self.params = args

    def __publish_path(self, msg):
        path_point = Point()
        path_point.x = msg.pose.position.x
        path_point.y = msg.pose.position.y
        path_point.z = 0.1
        self.marker.points.append(path_point)
        self.pub_path.publish(self.marker)
        if len(self.marker.points) > self.buff_len:
            self.marker.points.pop(0)


if __name__ == '__main__':
    main()
