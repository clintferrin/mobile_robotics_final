#!/usr/bin/env python
import rospy
import tf2_ros
import sys
import proj_sim.msg as vs
import geometry_msgs.msg as geo
from visualization_msgs.msg import Marker, MarkerArray
from BatteryModel import BatteryModel
from Parameters import Parameters
from tf.transformations import quaternion_from_euler


def main():
    # validate argument parameters
    if len(sys.argv) <= 3:
        args = {"tf_parent": "origin",
                "tf_child": "coil_markers",
                "csv_file_path": "../data/charging_coils.csv"}
    else:
        args = {"tf_parent": sys.argv[1],
                "tf_child": sys.argv[2],
                "csv_file_path": sys.argv[3]}

    coil_markers = PublishCoilMarkers(args)
    rospy.spin()


class PublishCoilMarkers:
    def __init__(self, args):
        rospy.init_node('coil_markers', anonymous=True)
        params = Parameters()
        csv_file_path = args["csv_file_path"]
        self.coils = BatteryModel(csv_file_path, params).coils
        self.pub_markers = rospy.Publisher("coil_markers", MarkerArray, queue_size=10)

        # setup array values
        tfBuffer = tf2_ros.Buffer()

        coil_markers = MarkerArray()
        coil_markers.markers = [Marker() for _ in range(len(self.coils) * 2)]

        rate = rospy.Rate(5)  # hz

        for idx, marker in enumerate(coil_markers.markers):
            marker.header.frame_id = str(args["tf_parent"])
            marker.frame_locked = True
            marker.id = idx
            marker.type = 1
            marker.header.stamp = rospy.Time.now()
            marker.color.r = .8
            marker.color.g = .8
            marker.color.b = .8
            marker.color.a = 1
            marker.action = 0

            marker.scale.x = 2 * self.coils[idx % len(self.coils)].x_charge_radius
            marker.scale.y = 2 * self.coils[idx % len(self.coils)].y_charge_radius
            marker.scale.z = .05

            q = quaternion_from_euler(0, 0, self.coils[idx % len(self.coils)].angle)
            marker.pose.orientation.x = q[0]
            marker.pose.orientation.y = q[1]
            marker.pose.orientation.z = q[2]
            marker.pose.orientation.w = q[3]

            marker.pose.position.x = self.coils[idx % len(self.coils)].x_pos
            marker.pose.position.y = self.coils[idx % len(self.coils)].y_pos
            marker.pose.position.z = 0

        for idx, coil in enumerate(self.coils):
            coil_markers.markers[idx].color.r = 0.1
            coil_markers.markers[idx].color.g = 0.1
            coil_markers.markers[idx].color.b = 0.1
            coil_markers.markers[idx].color.a = 0.4
            coil_markers.markers[idx].action = 0
            coil_markers.markers[idx].scale.x = 2 * (coil.x_charge_radius + coil.partial_charge_width)
            coil_markers.markers[idx].scale.y = 2 * (coil.y_charge_radius + coil.partial_charge_width)
            coil_markers.markers[idx].scale.z = .025
            coil_markers.markers[idx].pose.position.z = -0.1

        self.pub_markers.publish(coil_markers)

        while not rospy.is_shutdown():
            self.pub_markers.publish(coil_markers)
            rate.sleep()


if __name__ == '__main__':
    main()
