clear; close all; addpath functions;

%% Ackermann Steering Model
params.eps = 10;
L = 2.64922; params.L = L;  % default L=2
max_steer_angle = deg2rad(100); % default angle_limit=deg2rad(35)
max_velocity = 100; % default angle_limit=deg2rad(35)
max_change_steer_angle = 100;  % defalt angle_change_limit=0.5
max_accel_limit = 100;
[yd_inner, yd_dot_inner, yd_ddot_inner, theta_inner, v_inner, k_inner] = load_path("inner_path.mat");
[yd_outter, yd_dot_outter, yd_ddot_outter, theta_outter, v_outter, k_outter] = load_path("outter_path.mat");

% caculate k values using pole placement
I = eye(2);
Z = [0 0; 0 0];
A = [Z I; Z Z];
B = [Z; I];
rank(ctrb(A,B)); % is controllable

% initialize cost variables for Q and R
x_pos = 20;
y_pos = 20;
vel = 10;
omega = 10;
Q = diag([x_pos,y_pos,vel,omega]);

accel = 1;
ang_accel = 1;
R = diag([accel,ang_accel]);
R_inv = R^-1;

% calculate feedback matrix
P = are(A, B*R_inv*B', Q); % Solve ARE
K = R_inv*B'*P; 

% runge kutta 45 integration variables
rk4.dt = .01;
rk4.end_time = (length(yd_outter)-1)*rk4.dt;
rk4.time = 0:rk4.dt:rk4.end_time;
params.rk4 = rk4;

states = zeros(5,length(rk4.time));
input = zeros(2,length(rk4.time));
y_eps_states = zeros(2,length(rk4.time));

% initial conditions
phi = atan2(L*double(k_outter(1)),1);
x = [yd_inner(1,1); yd_inner(2,1); theta_inner(1); v_outter(1); phi];
y_eps_states(:,1) = [x(1); x(2)] + params.eps*[cos(x(3)); sin(x(3))];

for n=1:length(rk4.time)
    % update matrices for Z
    R_eps = [ cos(x(3)) -params.eps*sin(x(3)) 
              sin(x(3))  params.eps*cos(x(3)) ];

    w = x(4)*tan(x(5))/L;
         
    w_hat = [ 0         params.eps*w
              w/params.eps  0       ];

    y_eps = [x(1); x(2)] + params.eps*[cos(x(3)); sin(x(3))];
    
    y_eps_dot = R_eps*[x(4);w]; 
    
    % calcuate u
    Z = [y_eps; y_eps_dot] - [yd_outter(:,n); yd_dot_outter(:,n)];
    u_bar = yd_ddot_outter(:,n)-K*Z;
    a_bar = R_eps^-1*u_bar-w_hat*[x(4); w];
    
    % avoid divide by zero
    if x(4) == 0
        xi = 0;
    else
        xi = cos(x(5))^2*(L*a_bar(2)-a_bar(1)*tan(x(5)))/x(4);
    end
    
    if n == 41
        break_point = 1;
    end
    
    u = [a_bar(1); xi];
%     u = sat_inputs(u,max_accel_limit,max_change_steer_angle);

    % Update the state vectors with euler integration
    states(:,n) = x;
    input(:,n) = u;
    y_eps_states(:,n) = y_eps;    
    x = ode_rk4('acker_x_prop',x,u,0,params);
%     x = sat_states(x,max_velocity,max_steer_angle);
    
end


plot_trajectory(yd_inner,yd_outter,y_eps_states,states)
% plot_velocity(states,params);
% plot_pos_error_squared(rk4.time,states,yd_inner);
% plot_input(input, params);

% plot_real_time_controller(y_eps_states,yd_outter,params);
% plot_real_time_simulation(states,yd_outter,y_eps_states,params);



function plot_real_time_simulation(x,yd,y_eps,params)
    for t=0:params.rk4.dt*2:params.rk4.end_time
        % reference path
        plot(yd(1,round(1:t/params.rk4.dt)),yd(2,round(1:t/params.rk4.dt))); 
        hold on;

        % robot midpoint
        plot(x(1,round(1:t/params.rk4.dt)),x(2,round(1:t/params.rk4.dt)));

        % robot y_eps reference point
        plot(y_eps(1,round(1:t/params.rk4.dt)),y_eps(2,round(1:t/params.rk4.dt)));

        hold off;
        pause(params.rk4.dt)
    end
    h = legend('Desired Path','Robot Midpoint','Y_\epsilon');
    set(h,'Location','northwest');    
end

function plot_real_time_controller(y_eps,yd,params)
    for t=0:params.rk4.dt*2:params.rk4.end_time
        % reference path
        plot(yd(1,round(1:t/params.rk4.dt)),yd(2,round(1:t/params.rk4.dt))); 
        hold on;

        % robot y_eps reference point
        plot(y_eps(1,round(1:t/params.rk4.dt)),y_eps(2,round(1:t/params.rk4.dt)));

        hold off;
        pause(params.rk4.dt)
    end
    h = legend('Desired Path','Robot Midpoint','Y_\epsilon');
    set(h,'Location','northwest');    
end

function plot_pos_error_squared(t, traj, x)
    figure;
    x_error = traj(1,:) - x(1,:);
    y_error = traj(2,:) - x(2,:);
    total_error = sqrt(x_error.^2 + y_error.^2);
    plot(t,total_error);
end
function plot_input(input, params)
    % plot input
    figure;
    subplot(2,1,1)
    plot(params.rk4.time,input(1,:));
    ylabel("Throttle input (m/s^2)");

    subplot(2,1,2)
    plot(params.rk4.time,input(2,:));
    ylabel("Angular Velocity (m/s^2)");
    xlabel("Time (s)");
end

function plot_trajectory(yd_inner,yd_outter,y_eps_states,states)
    figure;
    hold on
    plot(yd_inner(1,:),yd_inner(2,:)) 
    plot(states(1,:),states(2,:));
    plot(y_eps_states(1,:),y_eps_states(2,:))
    plot(yd_outter(1,:),yd_outter(2,:))
    xlabel("x Position (m)");
    ylabel("y Position (m)");
    h = legend('Desired path','Center of back axel','Y_\epsilon_d','Y_\epsilon');
    set(h,'Location','northwest')
end

function plot_velocity(states, params)
    figure;
    plot(params.rk4.time,states(4,:))
    xlabel("time (s)");
    ylabel("velocity (m/s)");
end

function R = rotation(theta)
    R = [cos(theta) -sin(theta)
         sin(theta)  cos(theta)];
end

function [yd, yd_dot, yd_ddot, theta, v, k] = load_path(filename)
    path = load(filename);
    yd = [path.x;path.y];
    yd_dot = [path.x_dot;path.y_dot];
    yd_ddot = [path.x_ddot;path.y_ddot];
    theta = path.theta;
    v = path.v;
    k = path.k;
end

function x = sat_states(x,v_lim,phi_lim)
    % saturate v
    x(4) = min(v_lim, max(-v_lim, x(4)));
    
    % satruate phi
    x(5) = min(phi_lim, max(-phi_lim, x(5)));
end

function u = sat_inputs(u,a_lim,xi_lim)
    % saturate throttle
    u(1) = min(a_lim, max(-a_lim, u(1)));
    
    % satruate steering angle rate
    u(2) = min(xi_lim, max(-xi_lim, u(2)));
end

