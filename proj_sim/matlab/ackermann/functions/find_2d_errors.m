function [x_error, y_error] = find_2d_errors(pt_ref,pt_error,derivative)
    x_ref = cos(derivative);
    y_ref = sin(derivative);
    ref = [x_ref; y_ref];
    ref_perp = [-ref(2, 1); ref(1)];
    error = pt_error - pt_ref;
    x_error = error'*ref;
    y_error = error'*ref_perp;
    
end