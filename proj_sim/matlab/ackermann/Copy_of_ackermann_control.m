addpath kinematics
close all;
veh = {};
veh.eps = 1;
L = 2.64922; veh.L = L;  % default L=2
[yd_inner, yd_dot_inner, yd_ddot_inner, theta_inner, v_inner, k_inner] = load_path("inner_path.mat");
[yd_outter, yd_dot_outter, yd_ddot_outter, theta_outter, v_outter, k_outter] = load_path("outter_path.mat");
inner.yd = yd_inner;
inner.yd_dot = yd_dot_inner;
inner.yd_ddot = yd_ddot_inner;

outter.yd = yd_outter;
outter.yd_dot = yd_dot_outter;
outter.yd_ddot = yd_ddot_outter;

% caculate k values using pole placement
I = eye(2);
Z = [0 0; 0 0];
A = [Z I; Z Z];
B = [Z; I];
rank(ctrb(A,B)); % is controllable

% initialize cost variables for Q and R
x_pos = 20;
y_pos = 20;
vel = 10;
omega = 10;
Q = diag([x_pos,y_pos,vel,omega]);

accel = .1;
ang_accel = .1;
R = diag([accel,ang_accel]);
R_inv = R^-1;

% calculate feedback matrix
P = are(A, B*R_inv*B', Q); % Solve ARE
veh.K = R_inv*B'*P; 

% Integrate the state
t0 = 0; 
dt = 0.001; veh.dt = dt;
tf = 19.9;

% Create the vehicle and desired values
u = @(t,x)better_unicycle_controller(t,x,veh, outter);
phi = atan2(L*double(k_outter(1)),1);
x0 = [yd_inner(1,1); yd_inner(2,1); theta_inner(1); v_outter(1); phi];

integrator = @(t0, dt, tf, x0, veh, u) integrateODE(t0, dt, tf, x0, veh, u);
   
[tmat, xmat] = integrator(t0, dt, tf, x0, veh, u);

% Plot the velocities
% veh.plotVelocitiesAndInput(tmat, xmat, u);

% figure;
% plot(xmat(1,:),xmat(2,:));
% xlabel("x position");
% ylabel("y position");
plot_pos_error_squared(tmat,inner.yd,xmat);


function u = better_unicycle_controller(t, x, veh, traj)
    n = round(t/veh.dt) + 1;
    % update matrices for Z
    R_eps = [ cos(x(3)) -veh.eps*sin(x(3)) 
              sin(x(3))  veh.eps*cos(x(3)) ];

    w = x(4)*tan(x(5))/veh.L;
         
    w_hat = [ 0         veh.eps*w
              w/veh.eps  0       ];

    y_eps = [x(1); x(2)] + veh.eps*[cos(x(3)); sin(x(3))];
    
    y_eps_dot = R_eps*[x(4);w]; 
    
    % calcuate u
    Z = [y_eps; y_eps_dot] - [traj.yd(:,n); traj.yd_dot(:,n)];
    u_bar = traj.yd_ddot(:,n)-veh.K*Z;
    a_bar = R_eps^-1*u_bar-w_hat*[x(4); w];
    
    % avoid divide by zero
    if x(4) == 0
        xi = 0;
    else
        xi = cos(x(5))^2*(veh.L*a_bar(2)-a_bar(1)*tan(x(5)))/x(4);
    end
    u = [a_bar(1); xi];
end

function [tmat, xmat] = integrateODE(t0, dt, tf, x0, veh, u)
    % Integrate forward in time
    [tmat, xmat] = ode45(@(t,x)kinematics(veh, t, x, u(t,x)), t0:dt:tf, x0);
    
    % Transpose the outputs
    tmat = tmat';
    xmat = xmat';    
end

function xdot = kinematics(veh, t, x, u)
    xdot = [x(4)*cos(x(3))
            x(4)*sin(x(3))
            x(4)/veh.L*tan(x(5)) 
            u(1) 
            u(2)];
end 

function [yd, yd_dot, yd_ddot, theta, v, k] = load_path(filename)
    path = load(filename);
    yd = [path.x;path.y];
    yd_dot = [path.x_dot;path.y_dot];
    yd_ddot = [path.x_ddot;path.y_ddot];
    theta = path.theta;
    v = path.v;
    k = path.k;
end

function plot_pos_error_squared(t, traj, x)
    figure;
    x_size = size(x,2);
    x_error = traj(1,1:x_size) - x(1,:);
    y_error = traj(2,1:x_size) - x(2,1:x_size);
    total_error = sqrt(x_error.^2 + y_error.^2);
    plot(t,total_error);
end